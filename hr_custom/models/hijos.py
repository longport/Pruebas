# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from datetime import datetime, date
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import logging, time

_logger = logging.getLogger(__name__)

class Hijos(models.Model):
    _name = "hijos"

    name = fields.Char(string = "Nombre", required = True, readonly = False, help = "Nombre del Hijo") 
    document_num = fields.Char(string = "Documento de Identidad", required = True, help = "Documento de Identidad del Hijo") 
    scholarship = fields.Selection([
                                    ('preschool','Preescolar'),
                                    ('primary','Primaria'),
                                    ('bachelor','Bachiller'),
                                    ('professional','Profesional'),
                                    ('none','Ninguno')] ,
                                    string = "Escolaridad",
                                    help = "Escolaridad del Hijo",
                                    default = 'none',
                                    )
    date_born = fields.Date(string = "Fecha de Nacimiento", required = True, help = "Fecha de Nacimiento del Hijo")
    age = fields.Integer(string = "Edad", readonly=True, compute="_compute_age")
    parent = fields.Many2one('hr.employee', string = 'Padre')

    @api.onchange('date_born')
    def _compute_age(self):
        today = date.today()
        if self.date_born and (self.date_born < today):
            if (today.month == self.date_born.month) and (today.day < self.date_born.day):
                self.age = int((today.year - self.date_born.year)) -1
            elif today.month < self.date_born.month:
                self.age = int((today.year - self.date_born.year)) -1
            else:
                self.age = int((today.year - self.date_born.year))
        else:
            #Si la edad es negativa el campo se pone en blanco para que el usuario vuelva a ingresar la fecha de nacimiento
            self.date_born = '' 

    #Se verifica que el número de documento del hijo sea único
    _sql_constraints = [
        ('unique_document', 'UNIQUE(document_num)', 'El número de documento debe ser único'),
    ]
