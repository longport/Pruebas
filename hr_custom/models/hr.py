from odoo import models, fields, api

class Employee(models.Model):
    _inherit = "hr.employee"

    children_ids = fields.One2many('hijos', 'parent', 'Hijos')
    children = fields.Integer(compute="children_number")
    
    def children_number(self):
        self.children = len(self.children_ids) #se actualiza el número de hijos del empleado