# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Hr Custom',
    'version': '12.0',
    'category': 'Employees',
    'description': """Módulo que crea clase Hijos y se relaciona con los empleados""",
    "author": "Camilo Beltrán",
    'data': 
    [
        'views/hijos_views.xml',
        'views/hr_views.xml',
        'views/menu_item.xml',
        'security/ir.model.access.csv',
    ],
    'depends': [
        'hr',
    ],
    'images': ['static/src/img/logo.png'],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
