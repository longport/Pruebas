# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Hr Inherit',
    'version': '12.0',
    'category': 'Employees',
    'description': """ Modulo que hereda el modelo del modulo Hr y permite relacionar los empleados con los terceros """,
    "author": "Camilo Beltrán",
    'data': 
    [
        'views/hr_views.xml',
        'views/templates.xml',
    ],
    'depends': [
        'hr',
        'web',
    ],
    'images': ['static/src/img/logo.png'],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
