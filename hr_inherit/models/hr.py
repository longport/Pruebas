from odoo import models, fields, api, _
from odoo.exceptions import UserError
import logging

class Employee(models.Model):
    _inherit = "hr.employee"

    address_home_id = fields.Many2one(string="Tercero")
    rel_name = fields.Char(related='address_home_id.name')
    rel_vat = fields.Char(related='address_home_id.vat')
    rel_mobile = fields.Char(related='address_home_id.mobile')
    rel_email = fields.Char(related='address_home_id.email')
    rel_is_company = fields.Boolean(related='address_home_id.is_company')
    
    @api.onchange('address_home_id')
    def complete_fields(self):
        if not self.rel_is_company:
            self.name = self.rel_name
            self.identification_id = self.rel_vat
            self.mobile_phone = self.rel_mobile
            self.work_email = self.rel_email
    
    #Se verifica que el número de documento del empleado sea único
    _sql_constraints = [
        ('unique_document_third', 'UNIQUE(identification_id)', 'El número de documento debe ser único'),
    ]